# Words from UK station codes
[View as human-readable text](https://codeberg.org/HypedHelicopter/station-words/raw/branch/main/words_as_uk_station_codes.txt)

## JSON Output
[Get outputted JSON](https://codeberg.org/HypedHelicopter/station-words/raw/branch/main/words_as_uk_station_codes.json)

### JSON format
```json
{
  num_parts : [
    word : [code1, code2, ...]
    ...
  ]
  ...
}
```

## Data used
- Codes from <https://www.nationalrail.co.uk/stations_destinations/48541.aspx>
- Words from <https://github.com/dwyl/english-words>

## Run

```sh
python station_words.py
```
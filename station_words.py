import csv, json
from collections import defaultdict

# Config
MIN_NUM_PARTS = 1
MAX_NUM_PARTS = 0
OUTPUT_TXT = 1
OUTPUT_JSON = 0

MIN_WORD_LEN = MIN_NUM_PARTS*3
if MAX_NUM_PARTS > 0:
    MAX_WORD_LEN = MAX_NUM_PARTS*3

# Load codes (data from https://www.nationalrail.co.uk/stations_destinations/48541.aspx)
codes = {}
with open("codes.csv", "r") as csvfile:
    reader = csv.reader(csvfile)
    for row in list(reader)[1:]:
        codes[row[1]] = row[0]

# Load words (https://github.com/dwyl/english-words)
with open("words.txt", "r") as wordsfile:
    words = wordsfile.read().rstrip().split("\n")

# Find codes
encoded = defaultdict(lambda:defaultdict(set))

words = filter(
lambda word: all((
    len(word) % 3 == 0,
    MIN_WORD_LEN <= len(word),
    MAX_NUM_PARTS <= 0 or (MAX_NUM_PARTS > 0 and len(word) <= MAX_WORD_LEN))),
    words
)

for word in words:
    word_upper = word.upper()
    num_parts = len(word_upper)//3
    parts = []
    for n in range(num_parts):
        part = word_upper[3*n:3*(n+1)]
        if part in codes:
            parts.append(part)
        else:
            break

    if len(parts) == num_parts:
        encoded[num_parts][word] = parts

# output to file
if OUTPUT_JSON:
    with open("words_as_uk_station_codes.json", "w") as json_file:
        json.dump(encoded, json_file)

if OUTPUT_TXT:
    with open("words_as_uk_station_codes.txt", "w") as txt_file:
        codes_url, words_url, code_url = (
"https://www.nationalrail.co.uk/stations_destinations/48541.aspx",
"https://github.com/dwyl/english-words",
"https://codeberg.org/HypedHelicopter/station-words/")

        print(f"""Words as made up by UK station codes.  
Station Codes: {codes_url}  
Words: {words_url}  
Code that generated this: {code_url}  

---""", file=txt_file)
        for num_parts in encoded:
            print(f"\nN={num_parts}", file=txt_file)
            for word, parts in encoded[num_parts].items():
                print(word, "=", " -> ".join(parts), file=txt_file)
